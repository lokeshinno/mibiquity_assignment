package com.mobiquity.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import com.mobiquity.app.service.MobiquityService;

@SpringBootApplication
@EnableCaching
@EnableScheduling
public class MobiquityApplication implements CommandLineRunner {
	
	@Autowired
	private MobiquityService mobiquityService; 

	public static void main(String[] args) {
		SpringApplication.run(MobiquityApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		mobiquityService.getAllAtms();
	}
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}