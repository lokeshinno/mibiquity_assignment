package com.mobiquity.app.dto;
public class Address {
	private String street;
	private String housenumber;
	private String postalcode;
	private String city;
	GeoLocation GeoLocationObject;


	public String getStreet() {
		return street;
	}

	public String getHousenumber() {
		return housenumber;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public String getCity() {
		return city;
	}

	public GeoLocation getGeoLocation() {
		return GeoLocationObject;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public void setHousenumber(String housenumber) {
		this.housenumber = housenumber;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setGeoLocation(GeoLocation geoLocationObject) {
		this.GeoLocationObject = geoLocationObject;
	}
}