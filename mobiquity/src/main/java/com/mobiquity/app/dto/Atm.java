package com.mobiquity.app.dto;

import java.util.ArrayList;
import java.util.List;


public class Atm {
	Address AddressObject;
	private float distance;
	List <Object> openingHours = new ArrayList<>();
	private String functionality;
	private String type;

	public Address getAddress() {
		return AddressObject;
	}

	public float getDistance() {
		return distance;
	}

	public String getFunctionality() {
		return functionality;
	}

	public String getType() {
		return type;
	}

	public void setAddress(Address addressObject) {
		this.AddressObject = addressObject;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public void setFunctionality(String functionality) {
		this.functionality = functionality;
	}

	public void setType(String type) {
		this.type = type;
	}
}