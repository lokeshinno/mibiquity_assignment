package com.mobiquity.app.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mobiquity.app.dto.Atm;
import com.mobiquity.app.service.MobiquityService;

@RestController
@RequestMapping("/api/atm")
public class MobiquityController {	
	
	@Autowired
	private MobiquityService mobiquityService;

	@GetMapping("/all")
	public List<Atm> getAllAtms() {
		return mobiquityService.getAllAtms();
	}
	
	@GetMapping
	public List<Atm> getAtmsByCity(@RequestParam("city") String city) {
		return mobiquityService.getAllAtmsByCity(city);
	}
}