package com.mobiquity.app.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobiquity.app.dto.Atm;

@Service
public class MobiquityService {
	
	@Autowired
	private RestTemplate restTemplate;

	@Cacheable(cacheNames = {"atms"})
	public List<Atm> getAllAtms() {
		List<Atm> atms = null;
		String response = restTemplate.getForObject("https://www.ing.nl/api/locator/atms/", String.class);
		if(response!=null) {
			String atmsResponseJsonStr=response.substring(response.indexOf("\n")+1).trim();
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			try {
				atms = mapper.readValue(atmsResponseJsonStr, new TypeReference<List<Atm>>(){});
				System.out.println("Atms count:"+atms.size());
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
		return atms;
	}
	
	@Cacheable(cacheNames = "atmsByCity", key = "#city", condition = "#city.length>3")
	public List<Atm> getAllAtmsByCity(String city) {
		System.out.println("City : "+city);
		List<Atm> allAtms = getAllAtms();
		List<Atm> atmsByCity = allAtms.stream().filter(atm-> {
			return atm.getAddress().getCity().equalsIgnoreCase(city);
		}).collect(Collectors.toList());
		
		return atmsByCity;
	}
}
