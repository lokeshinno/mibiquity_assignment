package com.mobiquity.app.config;


import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
public class CacheConfig {

	@Bean
	public CacheManager cacheManager() {
		ConcurrentMapCacheManager cacheManager = new ConcurrentMapCacheManager("atms","atmsByCity");
		cacheManager.setAllowNullValues(false);
		return cacheManager;
	}
	
	@CacheEvict(allEntries = true, value = {"atms","atmsByCity"})
	@Scheduled(fixedDelay = 30* 60 * 1000 ,  initialDelay = 5 * 60 * 1000) //30mins and 5mins
	public void clearCacheEvictJob() {
		System.out.println("Removed old data");
	}
}