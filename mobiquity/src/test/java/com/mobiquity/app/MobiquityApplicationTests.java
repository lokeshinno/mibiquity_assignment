package com.mobiquity.app;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.mobiquity.app.service.MobiquityService;

@SpringBootTest
class MobiquityApplicationTests {
	
	@Autowired
	private MobiquityService service;
	
	@Test
	public void getAllAtmsTest() {
		assertTrue(service.getAllAtms().size()>0);
	}
	
	@Test
	public void getAllAtmsByCityTest() {
		System.out.println("size::"+service.getAllAtmsByCity("Zaltbommel").size());
		assertTrue(service.getAllAtmsByCity("Zaltbommel").size()>=1);
	}

}
